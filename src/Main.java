import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("First Name: ");
        String fname = scan.nextLine();

        System.out.print("Last Name: ");
        String lname = scan.nextLine();

        System.out.print("First Subject Grade: ");
        Double g1 = scan.nextDouble();

        System.out.print("Second Subject Grade: ");
        Double g2 = scan.nextDouble();

        System.out.print("Third Subject Grade: ");
        Double g3 = scan.nextDouble();

        Double ave = (g1+g2+g3)/3;
        System.out.println("Good Day, " + fname + " " + lname);
        System.out.println("Your Average is: " + ave);

    }
}